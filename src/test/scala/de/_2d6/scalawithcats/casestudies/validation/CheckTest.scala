package de._2d6.scalawithcats.casestudies.validation

import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{FlatSpec, Matchers}

import scala.util.Try

class CheckTest extends FlatSpec with Matchers {

  type Assertion[A, B] = Check[List[String], A, B]

  val isInteger: Assert[List[String], String] = Assert(s => Try(s.toInt).isSuccess, List("String must be convertable to an integer"))

  "A check" should "work for trivial cases" in {
    PredicateCheck(isInteger).validate("-1") shouldBe Valid("-1")

    PredicateCheck(isInteger).validate("A") shouldBe Invalid(isInteger.message)
  }

  it should "be able to map to a different data type" in {
    val check = PredicateCheck(isInteger).map(_.toInt)

    check.validate("-23") shouldBe Valid(-23)
    check.validate("") shouldBe Invalid(isInteger.message)
  }

  it should "be able to flatMap" in {
    val assertNonEmpty: Assert[List[String], String] = Assert(_.nonEmpty, List("String must not be empty"))
    val nonEmpty: Assertion[String, String] = PredicateCheck(assertNonEmpty)
    val intCheck: Assertion[String, Int] = PredicateCheck(isInteger).map(_.toInt)

    val check = nonEmpty.flatMap(_ => intCheck)

    check.validate("4") shouldBe Valid(4)
    check.validate("") shouldBe Invalid(assertNonEmpty.message)
    check.validate("A") shouldBe Invalid(isInteger.message)
  }

  it should "be composable via andThen" in {
    val assertLessThanFive: Assert[List[String], Int] = Assert(_ < 5, List("Number must be less than five"))
    val lessThanFive: Assertion[Int, Int] = PredicateCheck(assertLessThanFive)
    val intCheck: Assertion[String, Int] = PredicateCheck(isInteger).map(_.toInt)

    val check = intCheck andThen lessThanFive

    check.validate("A") shouldBe Invalid(isInteger.message)
    check.validate("5") shouldBe Invalid(assertLessThanFive.message)
    check.validate("4") shouldBe Valid(4)
  }
}
