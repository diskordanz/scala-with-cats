package de._2d6.scalawithcats.casestudies.validation

import cats.data.Validated.{Invalid, Valid}
import cats.instances.list._
import org.scalatest.{FlatSpec, Matchers}

class PredicateTest extends FlatSpec with Matchers {

  type IntPredicate = Predicate[List[String], Int]
  type IntAssert = Assert[List[String], Int]
  val lessThanFive: IntAssert = Assert(_ < 5, List("Value was not less than five"))
  val even: IntAssert = Assert(_ % 2 == 0, List("Value was not even"))

  "A predicate combination using AND" should "return the checked instance" in {
    val combined = lessThanFive and even

    combined.validate(4) shouldBe Valid(4)
  }

  it should "return an error message if one of the checks fails" in {
    val combined = lessThanFive and even

    combined.validate(3).isInvalid shouldBe true
    combined.validate(3) shouldBe Invalid(even.message)
  }

  it should "return all error messages if multiple checks fail" in {
    val combined = lessThanFive and even

    combined.validate(7).isInvalid shouldBe true
    combined.validate(7) shouldBe Invalid(lessThanFive.message ++ even.message)
  }

  "A predicate combination using OR" should "return the checked instance" in {
    val combined = lessThanFive or even

    combined.validate(4) shouldBe Valid(4)
  }

  it should "return Valid if the left check fails" in {
    val combined = lessThanFive or even

    combined.validate(6) shouldBe Valid(6)
  }

  it should "return Valid if the right check fails" in {
    val combined = lessThanFive or even

    combined.validate(3) shouldBe Valid(3)
  }

  it should "return an Invalid with both messages if both left and right fail" in {
    val combined = lessThanFive or even

    combined.validate(7).isInvalid shouldBe true
    combined.validate(7) shouldBe Invalid(lessThanFive.message ++ even.message)
  }
}
