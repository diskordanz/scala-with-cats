package de._2d6.scalawithcats.casestudies.mapreduce

import cats.Monoid
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class MapReduceTest extends FlatSpec with Matchers {

  implicit val monoid: WordCountMonoid = new WordCountMonoid

  private val word = "word"
  private val anotherWord = "anotherWord"
  
  "Map reduce" should "count words" in {
    val words = Vector(word, word, anotherWord)

    MapReduce.foldMap(words)(word => Map(word -> 1)) shouldBe Map(word -> 2, anotherWord -> 1)
  }

  it should "work with empty input" in {
    MapReduce.foldMap(Vector.empty)(identity) shouldBe empty
  }
  
  "Parallel map reduce" should "count words" in {
    val words = (1 to 3).flatMap(_ => Vector(word, word, anotherWord)).toVector
    
    await(MapReduce.parallelFoldMap(words)(word => Map(word -> 1))) shouldBe Map(word -> 6, anotherWord -> 3)
  }

  it should "work with empty input" in {
    await(MapReduce.parallelFoldMap(Vector.empty)(identity)) shouldBe empty
  }

  private def await[A](f: Future[A]): A = Await.result(f, 10.seconds)
}

class WordCountMonoid extends Monoid[Map[String, Int]] {
  override def empty: Map[String, Int] = Map.empty

  override def combine(counts: Map[String, Int], otherCounts: Map[String, Int]): Map[String, Int] = {
    val combinedKeys = counts.keySet ++ otherCounts.keySet
    combinedKeys.map(key => (key, counts.getOrElse(key, 0) + otherCounts.getOrElse(key, 0))).toMap
  }
}
