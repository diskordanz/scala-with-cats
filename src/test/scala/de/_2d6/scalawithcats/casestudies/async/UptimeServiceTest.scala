package de._2d6.scalawithcats.casestudies.async

import cats.instances.try_._
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Success, Try}

class UptimeServiceTest extends FlatSpec with Matchers {

  private val host1 = "host1"
  private val host2 = "host2"
  private val uptimeClient = new TestUptimeClient(Map(host1 -> 1, host2 -> 2))
  private val tryUptimeClient = new TryTestUptimeClient(Map(host1 -> Success(1), host2 -> Success(2)))

  "An uptime service" should "return the sum of uptimes" in {
    new UptimeService(uptimeClient).getTotalUptime(List(host1, host2)) shouldBe 3
  }

  it should "return zero if the list of hosts is empty" in {
    new UptimeService(uptimeClient).getTotalUptime(Nil) shouldBe 0
  }

  "An uptime service using Try" should "also work" in {
    new UptimeService(tryUptimeClient).getTotalUptime(List(host1, host2)) shouldBe Success(3)
  }

  it should "return a Failure if the client returns a failure" in {
    val brittleClient = new TryTestUptimeClient(Map(host1 -> Failure(new IllegalArgumentException("request failed")), host2 -> Success(2)))

    new UptimeService(brittleClient).getTotalUptime(List(host1, host2)) shouldBe a[Failure[_]]
  }

  class TryTestUptimeClient(uptimesByHostname: Map[String, Try[Int]]) extends UptimeClient[Try] {
    override def getUptime(hostname: String): Try[Int] = uptimesByHostname.getOrElse(hostname, Success(0))
  }

}
