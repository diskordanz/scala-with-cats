package de._2d6.scalawithcats.experiments

import de._2d6.scalawithcats.experiments.Writers.FilterLog
import org.scalatest.{FlatSpec, Matchers}

class WritersTest extends FlatSpec with Matchers {

  "A fizzbuzz filter" should "remove elements divisible by three or five" in {

    val input = List(1, 2, 3, 4, 5, 6, 9, 15)

    val output = Writers.fizzbuzzFilter(input)

    output.value shouldBe List(1, 2, 4)
  }

  it should "keep a log of removed elements" in {

    val input = List(1, 2, 3, 4, 5, 6, 9, 15)

    val output = Writers.fizzbuzzFilter(input)

    output.written shouldBe FilterLog(divisibleByThree = List(3, 6, 9, 15), divisibleByFive = List(5, 15))
  }
}
