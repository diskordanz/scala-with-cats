package de._2d6.scalawithcats.exercises.ex_4_6_5

import org.scalatest.{FlatSpec, Matchers}

class EvalFoldingTest extends FlatSpec with Matchers {

  "A folding operation" should "work even for large input" in {

    val largeList = List.fill(1000000)(1)

    val sum = EvalFolding.foldRight(largeList, 0)((elem, acc) => acc + elem)

    sum.value shouldBe largeList.length
  }
}
