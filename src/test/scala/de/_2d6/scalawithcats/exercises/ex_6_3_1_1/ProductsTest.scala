package de._2d6.scalawithcats.exercises.ex_6_3_1_1

import org.scalatest.{FlatSpec, Matchers}
import cats.instances.option._
import cats.instances.either._
import cats.instances.list._

class ProductsTest extends FlatSpec with Matchers {

  val option: Option[Int] = Some(1)
  val optionString: Option[String] = Some("string")
  val left: Either[String, Int] = Left("left")
  val rightLong: Either[String, Long] = Right(1)
  val rightSymbol: Either[String, Symbol] = Right('symbol)

  "A product method" should "combine options" in {
    Products.product(option, optionString) shouldBe Some((1, "string"))

    Products.product(optionString, None) shouldBe None
    Products.product(None, option) shouldBe None
    Products.product(None, None) shouldBe None
  }

  it should "combine Eithers" in {
    Products.product(rightLong, rightSymbol) shouldBe Right((1, 'symbol))

    Products.product(left, rightLong) shouldBe Left("left")
    Products.product(rightSymbol, left) shouldBe Left("left")
  }
  
  it should "combine lists" in {
    Products.product(List(1, 2), List(3, 4)) shouldBe List((1, 3), (1, 4), (2, 3), (2, 4))

    Products.product(List(1, 2), Nil) shouldBe Nil
    Products.product(Nil, List(1, 2)) shouldBe Nil
    Products.product(Nil, Nil) shouldBe Nil
  }
}
