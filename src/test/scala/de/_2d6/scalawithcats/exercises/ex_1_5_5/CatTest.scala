package de._2d6.scalawithcats.exercises.ex_1_5_5

import org.scalatest.{FlatSpec, Matchers}

class CatTest extends FlatSpec with Matchers {

  val cat = Cat("Herbert", 23, "black")
  val otherCat = Cat("Albert", 1, "white")

  "A cat" should "equal itself" in {
    (cat === cat) shouldBe true
    (cat !== cat) shouldBe false
  }

  it should "equal a copy of itself" in {
    (cat === cat.copy()) shouldBe true
    (cat !== cat.copy()) shouldBe false
  }

  it should "not equal another cat" in {
    (cat === otherCat) shouldBe false
    (cat !== otherCat) shouldBe true
  }

  "An optional cat" should "equal itself" in {
    val optionalCat = Some(cat)

    (optionalCat === optionalCat) shouldBe true
    (optionalCat !== optionalCat) shouldBe false
  }

  it should "equal a copy of itself" in {
    (Some(cat) === Some(cat.copy())) shouldBe true
    (Some(cat) !== Some(cat.copy())) shouldBe false
  }

  it should "equal another cat" in {
    (Some(cat) === Some(otherCat)) shouldBe false
    (Some(cat) !== Some(otherCat)) shouldBe true
  }

  it should "not equal an empty option" in {
    (Some(cat) === Option.empty) shouldBe false
    (Some(cat) !== Option.empty) shouldBe true
  }

}
