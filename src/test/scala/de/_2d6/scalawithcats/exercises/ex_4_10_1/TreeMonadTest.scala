package de._2d6.scalawithcats.exercises.ex_4_10_1

import cats.Monad
import org.scalatest.{FlatSpec, Matchers}
import cats.syntax.flatMap._
import cats.syntax.functor._

class TreeMonadTest extends FlatSpec with Matchers {

  import Tree._

  private val simpleTree: Tree[Int] = Leaf(1)
  private val complexTree: Tree[String] = Branch(Leaf("A"), Leaf("B"))

  val monad: Monad[Tree] = implicitly[Monad[Tree]]
  "A tree monad" should "create a tree" in {
    monad.pure(23) shouldBe Leaf(23)
    monad.pure("test") shouldBe Leaf("test")
  }

  it should "flatMap over a simple tree" in {
    monad.flatMap(simpleTree)(x => Leaf(x + 1)) shouldBe Leaf(2)
  }

  it should "flatMap over a complex tree" in {
    monad.flatMap(complexTree)(s => Branch(Leaf(s), Leaf(s))) shouldBe Branch(
      Branch(Leaf("A"), Leaf("A")),
      Branch(Leaf("B"), Leaf("B")))
  }

  it should "map over a simple tree" in {
    monad.map(simpleTree)(_ + 1) shouldBe Leaf(2)
  }

  it should "map over a complex tree" in {
    monad.map(complexTree)(s => s + s) shouldBe Branch(Leaf("AA"), Leaf("BB"))
  }

  it should "allow for comprehensions on trees" in {
    val result = for {
      value <- complexTree
      duplicated <- Leaf(value + value): Tree[String]
    } yield duplicated

    result shouldBe Branch(Leaf("AA"), Leaf("BB"))
  }
}
