package de._2d6.scalawithcats.exercises.ex_3_6_2_1

import org.scalatest.{FlatSpec, Matchers}

class CodecTest extends FlatSpec with Matchers {

  case class Box[A](value: A)

  val intCodec: Codec[Int] = Codec.StringCodec.imap(_.toInt, _.toString)

  "A codec" should "be transformable to other codecs" in {


    val encoded = intCodec.encode(42)
    val decoded = intCodec.decode(encoded)
    encoded shouldBe "42"
    decoded shouldBe 42
  }

  it should "be transformable to work with arbitrary objects" in {
    implicit val valueCodec: Codec[Int] = intCodec

    def codec[A](implicit valueCodec: Codec[A]): Codec[Box[A]] = {
      valueCodec.imap(Box(_), _.value)
    }

    val encoded = codec.encode(Box(666))
    val decoded = codec.decode(encoded)
    encoded shouldBe "666"
    decoded shouldBe Box(666)
  }
}
