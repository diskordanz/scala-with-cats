package de._2d6.scalawithcats.exercises.ex_4_9_3

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FlatSpec, Matchers}

class CalculatorTest extends FlatSpec with Matchers with TableDrivenPropertyChecks {

  val additions = Table(
    ("Stack", "Operator", "Expected result"),
    (List(1, 1), "+", 2),
    (List(0, 0), "+", 0),
    (List(1, -2), "+", -1),
    (List(1, 1), "-", 0),
    (List(1, 0), "-", -1),
    (List(0, 1), "-", 1),
    (List(1, 1), "*", 1),
    (List(1, 0), "*", 0),
    (List(-1, 1), "*", -1),
    (List(1, 1), "/", 1),
    (List(1, 0), "/", 0),
    (List(-1, 2), "/", -2),
    (List(2, -1), "/", 0),
    (List(2, -2), "/", -1)
  )

  "A calculator" should "evaluate operators" in {
    forAll(additions) { (stack, operator, expected) =>
      val resultState = Calculator.evalOne(operator).runA(stack).value

      resultState shouldBe expected
    }
  }

  val expressions = Table(
    ("Expression", "Expected result"),
    ("1 1 +", 2),
    ("2 1 - 3 +", 4),
    ("", 0),
    (" ", 0)
  )

  it should "evaluate expressions" in {
    forAll(expressions) { (expression, expected) =>
      Calculator.eval(expression) shouldBe expected
    }
  }

}
