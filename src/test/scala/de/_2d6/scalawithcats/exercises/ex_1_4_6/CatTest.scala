package de._2d6.scalawithcats.exercises.ex_1_4_6

import org.scalatest.{FlatSpec, Matchers}
import cats.syntax.show._

class CatTest extends FlatSpec with Matchers {

  val cat = Cat("catName", 5, "brown")

  "A cat" should "be showable" in {
    cat.show shouldBe "catName is a 5 year-old brown cat."
  }
}

