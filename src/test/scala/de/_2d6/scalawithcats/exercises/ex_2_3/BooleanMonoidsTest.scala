package de._2d6.scalawithcats.exercises.ex_2_3

import de._2d6.scalawithcats.exercises.ex_2_3.BooleanMonoids.{AndMonoid, NorMonoid, OrMonoid, XorMonoid}
import org.scalatest.{FlatSpec, Matchers}

class BooleanMonoidsTest extends FlatSpec with Matchers {

  "An AND monoid" should "be associative" in {
    val monoid = new AndMonoid

    monoid.combine(true, true) shouldBe true
    monoid.combine(true, false) shouldBe false
    monoid.combine(false, true) shouldBe false
    monoid.combine(false, false) shouldBe false
  }

  it should "have an empty element" in {
    val monoid = new AndMonoid

    monoid.combine(monoid.empty, true) shouldBe true
    monoid.combine(true, monoid.empty) shouldBe true
    monoid.combine(monoid.empty, false) shouldBe false
    monoid.combine(false, monoid.empty) shouldBe false
  }

  "An OR monoid" should "be associative" in {
    val monoid = new OrMonoid

    monoid.combine(true, true) shouldBe true
    monoid.combine(true, false) shouldBe true
    monoid.combine(false, true) shouldBe true
    monoid.combine(false, false) shouldBe false
  }

  it should "have an empty element" in {
    val monoid = new OrMonoid

    monoid.combine(monoid.empty, true) shouldBe true
    monoid.combine(true, monoid.empty) shouldBe true
    monoid.combine(monoid.empty, false) shouldBe false
    monoid.combine(false, monoid.empty) shouldBe false
  }

  "An XOR monoid" should "be associative" in {
    val monoid = new XorMonoid

    monoid.combine(true, true) shouldBe false
    monoid.combine(true, false) shouldBe true
    monoid.combine(false, true) shouldBe true
    monoid.combine(false, false) shouldBe false
  }

  it should "have an empty element" in {
    val monoid = new XorMonoid

    monoid.combine(monoid.empty, true) shouldBe true
    monoid.combine(true, monoid.empty) shouldBe true
    monoid.combine(monoid.empty, false) shouldBe false
    monoid.combine(false, monoid.empty) shouldBe false
  }

  "A NOR monoid" should "be associative" in {
    val monoid = new NorMonoid

    monoid.combine(true, true) shouldBe false
    monoid.combine(true, false) shouldBe false
    monoid.combine(false, true) shouldBe false
    monoid.combine(false, false) shouldBe true
  }

  it should "have an empty element" in {
    val monoid = new NorMonoid

    monoid.combine(monoid.empty, true) shouldBe false
    monoid.combine(true, monoid.empty) shouldBe false
    monoid.combine(monoid.empty, false) shouldBe true
    monoid.combine(false, monoid.empty) shouldBe true
  }
}
