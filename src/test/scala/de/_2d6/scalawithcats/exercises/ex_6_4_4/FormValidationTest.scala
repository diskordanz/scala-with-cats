package de._2d6.scalawithcats.exercises.ex_6_4_4

import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{FlatSpec, Matchers}

class FormValidationTest extends FlatSpec with Matchers {

  type Rejection = Invalid[List[String]]

  import FormValidation._

  "Form validation" should "read a valid name" in {
    readName(Map("name" -> "nameValue")) shouldBe Valid("nameValue")
  }

  it should "reject an empty name" in {
    readName(Map("name" -> " ")) shouldBe a[Rejection]
  }

  it should "reject a missing name" in {
    readName(Map.empty) shouldBe a[Rejection]
  }

  it should "read a valid age" in {
    readAge(Map("age" -> "23")) shouldBe Valid(23)
  }

  it should "reject an empty age" in {
    readAge(Map("age" -> " ")) shouldBe a[Rejection]
  }

  it should "reject a missing age" in {
    readAge(Map.empty) shouldBe a[Rejection]
  }

  it should "reject a negative age" in {
    readAge(Map("age" -> "-1")) shouldBe a[Rejection]
  }

  it should "accept a valid user" in {
    readUser(Map("name" -> "someName", "age" -> "42")) shouldBe Valid(User("someName", 42))
  }

  it should "reject an invalid user" in {
    readUser(Map.empty) shouldBe a[Rejection]
  }

  it should "reject an invalid user and return all errors" in {
    readUser(Map.empty).asInstanceOf[Rejection].e should have length 2
  }
}
