package de._2d6.scalawithcats.exercises.ex_5_4

import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._
import scala.concurrent.{Await, Awaitable}

class OptimusPrimeTest extends FlatSpec with Matchers {

  "An Optimus Prime" should "return a power level" in {
    val level = await(OptimusPrime.getPowerLevel("Bumblebee").value)

    level shouldBe Right(8)
  }

  it should "return an error message if it does not know the power level of a given autobot" in {
    val level = await(OptimusPrime.getPowerLevel("unknown").value)

    level shouldBe a[Left[_, _]]
    level.left.get.contains("unknown") shouldBe true
  }

  it should "allow two autobots to execute a special move" in {
    val mayExecute = await(OptimusPrime.canSpecialMove("Bumblebee", "Hot Rod").value)

    mayExecute shouldBe Right(true)
  }

  it should "not allow two autobots to execute a special move if their power level is insufficient" in {
    val mayExecute = await(OptimusPrime.canSpecialMove("Bumblebee", "Jazz").value)

    mayExecute shouldBe Right(false)
  }

  it should "not allow two autobots to execute a special move if one of them is not known" in {
    val mayExecute = await(OptimusPrime.canSpecialMove("Bumblebee", "unknown").value)

    mayExecute shouldBe a[Left[_, _]]
  }

  it should "not allow two autobots to execute a special move if both of them are not known" in {
    val mayExecute = await(OptimusPrime.canSpecialMove("unknown", "also not known").value)

    mayExecute shouldBe a[Left[_, _]]
  }

  it should "issue a tactical report" in {
    val report = OptimusPrime.tacticalReport("Bumblebee", "Hot Rod")

    report shouldBe "Bumblebee and Hot Rod may execute a special move"
  }

  it should "issue a tactical report if the power levels of two autobots are not sufficient" in {
    val report = OptimusPrime.tacticalReport("Bumblebee", "Jazz")

    report shouldBe "Bumblebee and Jazz may not execute a special move"
  }

  it should "issue a tactical report if one of two autobots is unknown" in {
    val report = OptimusPrime.tacticalReport("Bumblebee", "not known")

    report shouldBe "Bumblebee and not known may not execute a special move"
  }

  private def await[A](f: Awaitable[A]): A = Await.result(f, 10.seconds)
}
