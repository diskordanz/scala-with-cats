package de._2d6.scalawithcats.exercises.ex_4_2_1

import org.scalatest.{FlatSpec, Matchers}

class MonadTest extends FlatSpec with Matchers {

  "A monad" should "have a map method" in {

    val box = Box(23)

    Monad.BoxMonad.map(box)(_.toString) shouldBe Box("23")
  }
}
