package de._2d6.scalawithcats.exercises.ex_3_5_4

import org.scalatest.{FlatSpec, Matchers}

class TreeTest extends FlatSpec with Matchers {

  import cats.syntax.functor._

  "A tree" should "be mappable" in {
    val tree: Tree[Int] = Branch(Leaf(1), Branch(Leaf(2), Leaf(3)))

    tree.map(_.toString) shouldBe Branch(Leaf("1"), Branch(Leaf("2"), Leaf("3")))
  }

  it should "be mappable for arbitrary type parameters" in {
    val tree: Tree[String] = Branch(Leaf("1"), Branch(Leaf("2"), Leaf("3")))

    tree.map(_.toInt) shouldBe Branch(Leaf(1), Branch(Leaf(2), Leaf(3)))
  }
}
