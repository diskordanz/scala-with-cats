package de._2d6.scalawithcats.exercises.ex_1_3

import org.scalatest.{FlatSpec, Matchers}

class CatTest extends FlatSpec with Matchers {

  import Printables._

  val cat = Cat("catName", 5, "brown")
  "A cat" should "be printable" in {
    Printable.format(cat) shouldBe "catName is a 5 year-old brown cat."
  }

  it should "be converted to PrintableOps implicitly" in {
    cat.format shouldBe "catName is a 5 year-old brown cat."
  }
}
