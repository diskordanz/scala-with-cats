package de._2d6.scalawithcats.exercises.ex_4_7_3

import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class FactorialWriterTest extends FlatSpec with Matchers {

  import FactorialWriter._

  "A factorial" should "be calculated" in {
    factorial(5).value shouldBe 120
  }

  it should "be accompanied by the intermediate steps" in {
    factorial(5).written shouldBe Seq(1, 1, 2, 6, 24, 120)
  }

  it should "work when run in parallel" in {
    val result = Await.result(Future.sequence(Seq(
      Future(factorial(5)),
      Future(factorial(5))
    )), 10.seconds)

    result.map(_.value) shouldBe Seq(120, 120)
    result.map(_.written) shouldBe Seq(Seq(1, 1, 2, 6, 24, 120), Seq(1, 1, 2, 6, 24, 120))
  }
}
