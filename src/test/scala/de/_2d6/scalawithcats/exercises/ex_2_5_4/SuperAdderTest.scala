package de._2d6.scalawithcats.exercises.ex_2_5_4

import org.scalatest.{FlatSpec, Matchers}

class SuperAdderTest extends FlatSpec with Matchers {

  import cats.instances.int._
  import cats.instances.option._

  "A SuperAdder" should "add integers" in {

    SuperAdder.add(List(1)) shouldBe 1
    SuperAdder.add(List(1, 2, 3)) shouldBe 6
    SuperAdder.add(List(1, 0)) shouldBe 1
    SuperAdder.add(List(1, -1)) shouldBe 0
    SuperAdder.add(List(0, -1)) shouldBe -1
  }

  it should "add a List of optional integers" in {

    SuperAdder.add(List[Option[Int]](Some(1))) shouldBe Some(1)
    SuperAdder.add(List[Option[Int]](None)) shouldBe None
    SuperAdder.add(List[Option[Int]](Some(1), None)) shouldBe Some(1)
    SuperAdder.add(List[Option[Int]](Some(1), Some(2))) shouldBe Some(3)
    SuperAdder.add(List[Option[Int]](Some(1), Some(-1))) shouldBe Some(0)
    SuperAdder.add(List[Option[Int]](Some(1), Some(0))) shouldBe Some(1)
  }

  it should "add a List of Orders" in {
    val order1 = Order(23.42, 666.6)
    val order2 = Order(10.0, 1.0)

    SuperAdder.add(List(order1, order2)) shouldBe Order(33.42, 667.6)
  }
}
