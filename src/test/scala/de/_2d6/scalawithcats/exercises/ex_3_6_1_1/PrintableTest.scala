package de._2d6.scalawithcats.exercises.ex_3_6_1_1

import org.scalatest.{FlatSpec, Matchers}

class PrintableTest extends FlatSpec with Matchers {

  "A Printable" should "possess a contramap method" in {

    implicit val printable: Printable[Int] = _.toString

    val contramapped = printable.contramap((to: Box[Int]) => to.value)

    contramapped.format(Box(23)) shouldBe "23"
  }

  it should "work with arbitrary boxes" in {

    implicit val valuePrintable: Printable[Int] = _.toString

    val boxPrintable: Printable[Box[Int]] = Printable.boxedPrintable

    boxPrintable.format(Box(42)) shouldBe "Box(42)"
  }
}
