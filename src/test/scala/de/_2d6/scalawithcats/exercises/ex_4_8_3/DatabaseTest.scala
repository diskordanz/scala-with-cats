package de._2d6.scalawithcats.exercises.ex_4_8_3

import org.scalatest.{FlatSpec, Matchers}

class DatabaseTest extends FlatSpec with Matchers {

  val database = Database(Map(1 -> "herbert"), Map("herbert" -> "valid"))

  "A Database" should "find a username" in {
    val reader = Database.findUsername(1)

    reader(database) shouldBe Some("herbert")
  }

  it should "return None if no username is found" in {
    val reader = Database.findUsername(666)

    reader(database) shouldBe None
  }

  it should "return true on valid passwords" in {
    val reader = Database.checkPassword("herbert", "valid")

    reader(database) shouldBe true
  }

  it should "return false on invalid passwords" in {
    val reader = Database.checkPassword("herbert", "not valid")

    reader(database) shouldBe false
  }

  it should "accept a valid user login" in {
    val reader = Database.checkLogin(1, "valid")

    reader(database) shouldBe true
  }

  it should "reject a nonexistent user" in {
    val reader = Database.checkLogin(42, "valid")

    reader(database) shouldBe false
  }

  it should "reject a login with invalid password" in {
    val reader = Database.checkLogin(1, "not valid")

    reader(database) shouldBe false
  }

}
