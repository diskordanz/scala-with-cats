package de._2d6.scalawithcats.exercises.ex_7_1_3

import org.scalatest.{FlatSpec, Matchers}

class FoldsTest extends FlatSpec with Matchers {

  "A map function" should "work the same as the builtin" in {
    val f = (i: Int) => i + 1

    Folds.map(List(1, 2, 3))(f) shouldBe List(2, 3, 4)
    Folds.map(List.empty[Int])(f) shouldBe empty
  }

  "A flatMap function" should "work the same as the builtin" in {
    val f = (i: Int) => List(i, i)

    Folds.flatMap(List(1))(f) shouldBe List(1, 1)
    Folds.flatMap(List.empty[Int])(f) shouldBe empty
  }

  "A filter function" should "work the same as the builtin" in {
    val predicate = (i: Int) => i % 2 == 0

    Folds.filter(List(1, 2, 3))(predicate) shouldBe List(2)
    Folds.filter(List.empty[Int])(predicate) shouldBe empty
  }

  "A sum function" should "work the same as the builtin" in {
    Folds.sum(List(1, 2, 3)) shouldBe 6
    Folds.sum(List.empty[Double]) shouldBe 0
  }
}
