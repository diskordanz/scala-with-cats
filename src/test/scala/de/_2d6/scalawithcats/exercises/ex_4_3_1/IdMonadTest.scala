package de._2d6.scalawithcats.exercises.ex_4_3_1

import org.scalatest.{FlatSpec, Matchers}

class IdMonadTest extends FlatSpec with Matchers {

  "An Id monad" should "have a flatMap method" in {

    val monad = new IdMonad

    val mapped = monad.flatMap(23)(_ + 1)

    mapped shouldBe 24
  }

}
