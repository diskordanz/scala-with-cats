package de._2d6.scalawithcats.experiments

import cats.data.Writer

object Writers {

  case class FilterLog(divisibleByThree: Seq[Int], divisibleByFive: Seq[Int]) {
    def addDivisibleByThree(int: Int): FilterLog = copy(divisibleByThree = int +: divisibleByThree)
    def addDivisibleByFive(int: Int): FilterLog = copy(divisibleByFive = int +: divisibleByFive)
  }

  object FilterLog {
    def empty = FilterLog(Nil, Nil)
  }

  def fizzbuzzFilter(list: List[Int]): Writer[FilterLog, Seq[Int]] = list match {
    case x :: xs if divisibleBy(x, 3) && divisibleBy(x, 5) => fizzbuzzFilter(xs).mapWritten(log => log.addDivisibleByThree(x).addDivisibleByFive(x))
    case x :: xs if divisibleBy(x, 3) => fizzbuzzFilter(xs).mapWritten(log => log.addDivisibleByThree(x))
    case x :: xs if divisibleBy(x, 5) => fizzbuzzFilter(xs).mapWritten(log => log.addDivisibleByFive(x))
    case x :: xs => fizzbuzzFilter(xs).map(x +: _)
    case Nil => Writer(FilterLog.empty, Nil)
  }

  private def divisibleBy(x: Int, divisor: Int) = x % divisor == 0
}