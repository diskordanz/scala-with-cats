package de._2d6.scalawithcats.exercises.ex_4_3_1

import de._2d6.scalawithcats.ex_4_3_1.Id


class IdMonad extends Monad[Id] {
  override def pure[A](x: A): Id[A] = x

  override def flatMap[A, B](a: Id[A])(f: A => Id[B]): Id[B] = f(a)

  override def map[A, B](a: Id[A])(f: A => B): Id[B] = f(a)
}
