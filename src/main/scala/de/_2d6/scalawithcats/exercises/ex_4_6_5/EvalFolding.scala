package de._2d6.scalawithcats.exercises.ex_4_6_5

import cats.Eval

object EvalFolding {

  def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): Eval[B] =
    as match {
      case head :: tail =>
        Eval.defer(foldRight(tail, acc)(fn))
          .map(fn(head, _))
      case Nil =>
        Eval.now(acc)
    }
}
