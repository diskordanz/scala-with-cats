package de._2d6.scalawithcats.exercises.ex_6_4_4

import cats.Semigroupal
import cats.data.Validated
import cats.data.Validated.{valid, invalid}
import cats.instances.list._

object FormValidation {

  type Form = Map[String, String]
  type ErrorsOr[A] = Validated[List[String], A]

  private object Keys {
    val Name = "name"
    val Age = "age"
  }

  case class User(name: String, age: Int)

  def readUser(form: Form): ErrorsOr[User] = Semigroupal[ErrorsOr].product(
      readName(form),
      readAge(form)
    ).map(User.tupled)

  def readName(form: Form): ErrorsOr[String] = read(Keys.Name, form)
    .andThen(notEmpty(Keys.Name, _))

  def readAge(form: Form): ErrorsOr[Int] = {
    read(Keys.Age, form)
      .andThen(notEmpty(Keys.Age, _))
      .andThen(parseInt(Keys.Age, _))
      .andThen(nonNegative(Keys.Age, _))
  }

  private def notEmpty(key: String, value: String): ErrorsOr[String] = if (value.isEmpty) {
    invalid(List(s"'$key' must not be empty"))
  } else {
    valid(value)
  }

  private def parseInt(key: String, value: String): ErrorsOr[Int] = try {
    valid(value.toInt)
  } catch {
    case _: Throwable => invalid(List(s"'$key' did not contain a valid integer"))
  }

  private def nonNegative(key: String, value: Int): ErrorsOr[Int] = if (value < 0) {
    invalid(List(s"'$key' must not contain a negative integer"))
  } else {
    valid(value)
  }

  private def read(key: String, form: Form): ErrorsOr[String] = form.get(key)
    .map(s => valid(s.trim))
    .getOrElse(invalid(List(s"Must contain '${Keys.Name}'")))

}