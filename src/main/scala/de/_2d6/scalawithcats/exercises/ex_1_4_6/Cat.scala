package de._2d6.scalawithcats.exercises.ex_1_4_6

import cats.Show

final case class Cat(name: String, age: Int, color: String)

object Cat {

  import cats.instances.string._
  import cats.instances.int._
  import cats.syntax.show._

  implicit def show: Show[Cat] = (c: Cat) =>{
    val name  = c.name.show
    val age   = c.age.show
    val color = c.color.show
    s"$name is a $age year-old $color cat."
  }
}