package de._2d6.scalawithcats.exercises.ex_4_2_1

import scala.language.higherKinds

trait Monad[F[_]] {
  def pure[A](a: A): F[A]

  def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]

  def map[A, B](value: F[A])(func: A => B): F[B] = flatMap(value)(func.andThen(pure))
}

object Monad {

  implicit def BoxMonad: Monad[Box] = new Monad[Box] {
    override def pure[A](a: A): Box[A] = Box(a)

    override def flatMap[A, B](box: Box[A])(func: A => Box[B]): Box[B] = func(box.value)
  }
}

case class Box[A](value: A)