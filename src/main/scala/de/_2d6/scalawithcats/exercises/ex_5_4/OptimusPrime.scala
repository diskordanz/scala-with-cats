package de._2d6.scalawithcats.exercises.ex_5_4

import cats.data.EitherT
import cats.instances.future._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object OptimusPrime {

  type Response[A] = EitherT[Future, String, A]

  private val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10
  )

  def getPowerLevel(autobot: String): Response[Int] = {
    powerLevels.get(autobot) match {
      case Some(powerLevel) => EitherT.right[String](Future.successful(powerLevel))
      case None => EitherT.left[Int](Future.successful(s"Autobot $autobot unreachable"))
    }
  }

  def canSpecialMove(ally1: String, ally2: String): Response[Boolean] = {
    for {
      powerLevel1 <- getPowerLevel(ally1)
      powerLevel2 <- getPowerLevel(ally2)
    } yield powerLevel1 + powerLevel2 > 15
  }

  def tacticalReport(ally1: String, ally2: String): String = {
    val maySpecialMove = Await.result(canSpecialMove(ally1, ally2).value, 1.seconds)
    maySpecialMove match {
      case Right(true) => s"$ally1 and $ally2 may execute a special move"
      case _ => s"$ally1 and $ally2 may not execute a special move"
    }
  }
}
