package de._2d6.scalawithcats.exercises.ex_4_9_3

import java.util.function.BinaryOperator

import cats.data.State

object Calculator {
  type CalcState[A] = State[List[A], A]

  private val noopState = State[List[Int], Int]((_, 0))

  def eval(expression: String): Int = {
    val states = toSymbolArray(expression).map(evalOne)
    val result = states.foldLeft(noopState)(concatenate)
    result.runA(List.empty).value
  }

  private def toSymbolArray(expression: String): Array[String] = {
    expression.split(" ")
      .map(_.trim)
      .filter(_.nonEmpty)
  }

  private def concatenate[A](s1: CalcState[A], s2: CalcState[A]) = s1.flatMap(_ => s2)

  def evalOne(symbol: String): CalcState[Int] = {
    symbol match {
      case "+" => operator(_ + _)
      case "-" => operator(_ - _)
      case "*" => operator(_ * _)
      case "/" => operator(_ / _)
      case num => operand(num.toInt)
    }
  }

  private def operator(op: BinaryOperator[Int]): CalcState[Int] = State[List[Int], Int] {
    case operand2 :: operand1 :: stack =>
      val result = op(operand1, operand2)
      (result :: stack, result)
    case _ => throw new IllegalArgumentException("Not enough elements on stack")
  }

  private def operand(int: Int): CalcState[Int] = State[List[Int], Int] { oldState =>
    (int :: oldState, int)
  }
}
