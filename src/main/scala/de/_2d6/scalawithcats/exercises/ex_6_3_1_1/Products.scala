package de._2d6.scalawithcats.exercises.ex_6_3_1_1

import cats.Monad
import cats.syntax.flatMap._
import cats.syntax.functor._

object Products {


  def product[M[_] : Monad, A, B](x: M[A], y: M[B]): M[(A, B)] = {
    for {
      xval <- x
      yval <- y
    } yield (xval, yval)
  }
}
