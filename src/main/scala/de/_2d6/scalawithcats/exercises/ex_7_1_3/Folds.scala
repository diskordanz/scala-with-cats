package de._2d6.scalawithcats.exercises.ex_7_1_3

object Folds {

  def map[A, B](list: List[A])(f: A => B): List[B] = list.foldRight(List.empty[B])((x, xs) => f(x) +: xs)

  def flatMap[A, B](list: List[A])(f: A => List[B]): List[B] = list.foldRight(List.empty[B])((x, xs) => f(x) ++ xs)

  def filter[A](list: List[A])(f: A => Boolean) : List[A] = flatMap(list)(x => if (f(x)) List(x) else Nil)

  def sum[A](list: List[A])(implicit ev: Numeric[A]): A = list.foldRight(ev.zero)(ev.plus)
}
