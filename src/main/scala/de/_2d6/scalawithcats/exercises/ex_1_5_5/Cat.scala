package de._2d6.scalawithcats.exercises.ex_1_5_5

import cats.Eq

final case class Cat(name: String, age: Int, color: String)

object Cat {

  implicit val catEqual: Eq[Cat] = (cat: Cat, other: Cat) => cat == other
}