package de._2d6.scalawithcats.exercises.ex_4_10_1

import cats.Monad

sealed trait Tree[+A]

object Tree {

  implicit def treeMonad: Monad[Tree] = new Monad[Tree] {
    override def flatMap[A, B](fa: Tree[A])(f: A => Tree[B]): Tree[B] = fa match {
      case Branch(left, right) => Branch(flatMap(left)(f), flatMap(right)(f))
      case Leaf(value) => f(value)
    }

    /*
    This method is not stack safe and thus violates the contract of tailRecM.
     */
    override def tailRecM[A, B](a: A)(f: A => Tree[Either[A, B]]): Tree[B] = {
      flatMap(f(a)) {
        case Left(newA) => tailRecM(newA)(f)
        case Right(b) => Leaf(b)
      }
    }

    override def pure[A](x: A): Tree[A] = Leaf(x)
  }
}

final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

final case class Leaf[A](value: A) extends Tree[A]