package de._2d6.scalawithcats.exercises.ex_3_5_4

import cats.Functor

sealed trait Tree[+A]

object Tree {

  implicit def treeFunctor: Functor[Tree] = new Functor[Tree] {
    override def map[A, B](node: Tree[A])(f: A => B): Tree[B] = node match {
      case Leaf(value) => Leaf(f(value))
      case Branch(left, right) => Branch(treeFunctor.map(left)(f), treeFunctor.map(right)(f))
    }
  }
}

final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

final case class Leaf[A](value: A) extends Tree[A]