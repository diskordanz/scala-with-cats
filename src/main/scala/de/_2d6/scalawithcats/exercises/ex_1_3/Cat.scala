package de._2d6.scalawithcats.exercises.ex_1_3

final case class Cat(name: String, age: Int, color: String)

object Cat {
  implicit def printable(implicit intFormat: Printable[Int], stringFormat: Printable[String]): Printable[Cat] =
    (c: Cat) => s"${stringFormat.format(c.name)} is a ${intFormat.format(c.age)} year-old ${stringFormat.format(c.color)} cat."
}