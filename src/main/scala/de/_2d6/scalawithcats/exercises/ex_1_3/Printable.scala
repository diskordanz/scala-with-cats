package de._2d6.scalawithcats.exercises.ex_1_3

trait Printable[A] {

  def format(a: A): String
}

object Printable {

  def format[A: Printable](a: A): String = implicitly[Printable[A]].format(a)
  def print[A: Printable](a: A): Unit = println(format(a))
}

object Printables {

  implicit val intPrintable: Printable[Int] = (a: Int) => a.toString
  implicit val stringPrintable: Printable[String] = (a: String) => a

  implicit class PrintableOps[A: Printable](wrapped: A) {

    def format: String = Printable.format(wrapped)
    def print(): Unit = Printable.print(wrapped)
  }
}