package de._2d6.scalawithcats.exercises.ex_2_3

object BooleanMonoids {

  class AndMonoid extends Monoid[Boolean] {
    override val empty: Boolean = true

    override def combine(x: Boolean, y: Boolean): Boolean = x && y
  }

  class OrMonoid extends Monoid[Boolean] {
    override val empty: Boolean = false

    override def combine(x: Boolean, y: Boolean): Boolean = x || y
  }

  class XorMonoid extends Monoid[Boolean] {
    override val empty: Boolean = false

    override def combine(x: Boolean, y: Boolean): Boolean = x ^ y
  }

  class NorMonoid extends Monoid[Boolean] {
    override def empty: Boolean = false

    override def combine(x: Boolean, y: Boolean): Boolean = !(x || y)
  }
}
