package de._2d6.scalawithcats.exercises.ex_3_6_2_1

trait Codec[A] { self =>
  def encode(value: A): String

  def decode(value: String): A

  def imap[B](dec: A => B, enc: B => A): Codec[B] = new Codec[B] {
    override def encode(value: B): String = self.encode(enc(value))

    override def decode(value: String): B = dec(self.decode(value))
  }
}

object Codec {

  implicit val StringCodec: Codec[String] = new Codec[String] {
      def encode(value: String): String = value

      def decode(value: String): String = value
    }
}