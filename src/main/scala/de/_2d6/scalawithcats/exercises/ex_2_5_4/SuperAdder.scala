package de._2d6.scalawithcats.exercises.ex_2_5_4

import cats.kernel.Monoid
import cats.syntax.monoid._

case class Order(totalCost: Double, quantity: Double)

object Order {
  val empty = Order(0, 0)

  implicit val orderAddition: Monoid[Order] = new Monoid[Order] {
    override def empty: Order = Order.empty

    override def combine(x: Order, y: Order): Order = Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
  }
}

object SuperAdder {

  def add[A](list: List[A])(implicit monoid: Monoid[A]): A = list.foldLeft(monoid.empty)((a, b) => a |+| b)
}