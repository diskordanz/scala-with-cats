package de._2d6.scalawithcats.exercises.ex_4_7_3

import cats.data.Writer

object FactorialWriter {

  def slowly[A](body: => A): A =
    try body finally Thread.sleep(100)

  def factorial(n: Int): Writer[Seq[Int], Int] = slowly {
    if (n == 0) {
      Writer(Seq(1), 1)
    } else {
      val res = factorial(n - 1).map(_ * n)
      res.mapWritten(_ :+ res.value)
    }
  }
}
