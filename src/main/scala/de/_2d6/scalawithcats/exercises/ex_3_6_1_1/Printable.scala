package de._2d6.scalawithcats.exercises.ex_3_6_1_1

trait Printable[A] {
  def format(value: A): String

  def contramap[B](func: B => A): Printable[B] = {
    value: B => this.format(func(value))
  }
}

object Printable {

  def boxedPrintable[A](implicit valuePrintable: Printable[A]): Printable[Box[A]] = {
    box: Box[A] => s"Box(${valuePrintable.format(box.value)})"
  }
}

case class Box[A](value: A)
