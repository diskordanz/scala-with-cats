package de._2d6.scalawithcats.exercises.ex_4_8_3

import cats.data.Reader

case class Database(users: Map[Int, String], passwords: Map[String, String])

object Database {
  type DbReader[A] = Reader[Database, A]

  def findUsername(userId: Int): DbReader[Option[String]] = {
    Reader(db => db.users.get(userId))
  }

  def checkPassword(username: String, password: String): DbReader[Boolean] = {
    Reader(db => db.passwords.get(username).contains(password))
  }

  def checkLogin(userId: Int, password: String): DbReader[Boolean] = {
    findUsername(userId).flatMap(maybeName =>
      maybeName.map(name => checkPassword(name, password))
        .getOrElse(Reader(_ => false)))
  }
}
