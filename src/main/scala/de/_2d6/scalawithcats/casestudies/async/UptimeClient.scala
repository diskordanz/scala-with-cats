package de._2d6.scalawithcats.casestudies.async

import cats.Id

import scala.concurrent.Future

trait UptimeClient[F[_]] {
  def getUptime(hostname: String): F[Int]
}

trait RealUptimeClient extends UptimeClient[Future] {
  def getUptime(hostname: String): Future[Int]
}

class TestUptimeClient(uptimesByHostname: Map[String, Int]) extends UptimeClient[Id] {
  def getUptime(hostname: String): Int = uptimesByHostname.getOrElse(hostname, 0)
}
