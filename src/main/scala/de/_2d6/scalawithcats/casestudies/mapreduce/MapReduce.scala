package de._2d6.scalawithcats.casestudies.mapreduce

import cats.Monoid

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object MapReduce {

  private val cpuCount: Int = Runtime.getRuntime.availableProcessors

  def foldMap[A, B](values: Vector[A])(f: A => B)(implicit monoid: Monoid[B]): B = {
    monoid.combineAll(values.map(f))
  }

  def parallelFoldMap[A, B](values: Vector[A])(f: A => B)(implicit monoid: Monoid[B]): Future[B] = {
    Future.sequence(group(values)
      .map(group => Future(foldMap(group)(f))))
      .map(monoid.combineAll)
  }

  private def group[A](values: Vector[A]): Vector[Vector[A]] = {
    val groupSize = (values.size.toDouble / cpuCount).ceil.toInt.max(1)
    values.grouped(groupSize).toVector
  }

}
