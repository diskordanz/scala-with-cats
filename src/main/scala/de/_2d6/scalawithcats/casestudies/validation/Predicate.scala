package de._2d6.scalawithcats.casestudies.validation

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.kernel.Semigroup

sealed trait Predicate[E, A] {

  type Result = Validated[E, A]

  def validate(value: A): Result
}

object Predicate {
  implicit class CheckOps[E: Semigroup, A](wrapped: Predicate[E, A]) {
    def and(other: Predicate[E, A]): Predicate[E, A] = And(wrapped, other)

    def or(other: Predicate[E, A]): Predicate[E, A] = Or(wrapped, other)
  }
}

case class And[E: Semigroup, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A] {
  override def validate(value: A): Result = (left.validate(value), right.validate(value)) match {
    case (Valid(a), Valid(_)) => Valid(a)
    case (Invalid(leftE), Invalid(rightE)) => Invalid(Semigroup[E].combine(leftE, rightE))
    case (Invalid(e), _) => Invalid(e)
    case (_, Invalid(e)) => Invalid(e)
  }
}

case class Or[E: Semigroup, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A] {
  override def validate(value: A): Result = (left.validate(value), right.validate(value)) match {
    case (Valid(a), _) => Valid(a)
    case (_, Valid(a)) => Valid(a)
    case (Invalid(leftE), Invalid(rightE)) => Invalid(Semigroup[E].combine(leftE, rightE))
  }
}

case class Assert[E, A](assertion: A => Boolean, message: E) extends Predicate[E, A] {
  override def validate(value: A): Result = if (assertion(value)) Valid(value) else Invalid(message)
}