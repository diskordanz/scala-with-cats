package de._2d6.scalawithcats.casestudies.validation

import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}

sealed trait Check[E, A, B] {
  def validate(a: A): Validated[E, B]

  def map[C](f: B => C): Check[E, A, C] = Map[E, A, B, C](this, f)

  def flatMap[C](f: B => Check[E, A, C]): Check[E, A, C] = FlatMap[E, A, B, C](this, f)

  def andThen[C](that: Check[E, B, C]): Check[E, A, C] = AndThen(this, that)
}

case class PredicateCheck[E, A](predicate: Predicate[E, A]) extends Check[E, A, A] {
  override def validate(a: A): Validated[E, A] = predicate.validate(a)
}

case class Map[E, A, B, C](check: Check[E, A, B], f: B => C) extends Check[E, A, C] {
  override def validate(a: A): Validated[E, C] = check.validate(a) match {
    case Valid(b) => Valid(f(b))
    case Invalid(e) => Invalid(e)
  }
}

case class FlatMap[E, A, B, C](check: Check[E, A, B], f: B => Check[E, A, C]) extends Check[E, A, C] {
  override def validate(a: A): Validated[E, C] = check.validate(a) match {
    case Valid(b) => f(b).validate(a)
    case Invalid(e) => Invalid(e)
  }
}

case class AndThen[E, A, B, C](first: Check[E, A, B], second: Check[E, B, C]) extends Check[E, A, C] {
  override def validate(a: A): Validated[E, C] = first.validate(a) match {
    case Valid(b) => second.validate(b)
    case Invalid(e) => Invalid(e)
  }
}